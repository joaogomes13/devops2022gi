﻿using System.Collections.Generic;
using Code.Contexts;
using Code.Models;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Code.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BuildingController : ControllerBase
    {

         private BackendContext context;

        public BuildingController()
        {
             context = new BackendContext();
          
        }


        /// <summary>
        ///  Get all the buildings
        /// </summary>
        /// <response code="200">Sucess</response>
        /// <response code="500">Error</response>
        ///
        [HttpGet]
        public async Task<IEnumerable<Building>> Index()    
        {

             List<Building> buildings = new List<Building>();

             buildings = await Task.Run( () => context.Buildings.ToList());

             return buildings;

        }

          /// <summary>
        ///  Get a specific building by ID
        /// </summary>
        /// <param name="id"> Building ID</param>
        /// <response code="200">Sucess</response>
        /// <response code="400">Not found</response>
        /// <response code="500">Error</response>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id){

           Building building =  await Task.Run(() =>  context.Buildings.Find(id));

            if(building == null)
                return NotFound();
            
            return Ok(building) ;

        }



        /// <summary>
        /// Create Building
        /// </summary>
        /// <param name="building"> Building Model</param>
        /// <response code="200">Sucess</response>
        /// <response code="400">Invalid Building Model</response>
        /// <response code="404">Not found</response>
        /// <response code="500">Error</response>
        [HttpPost]
        public async Task<ActionResult> Create(Building building){

            await Task.Run(() =>  context.Buildings.Add(building));
            context.SaveChanges();
            return CreatedAtAction(
                nameof(Create),
                new {
                id = building.BuildingId
                },
                building
            );

        }



        /// <summary>
        /// Edit  specific building by ID
        /// </summary>
        /// <param name="id"> Building ID</param>
        /// <param name="building"> Building Model </param>
        /// <response code="200">Sucess</response>
        /// <response code="400">Invalid Building Model</response>
        ///  <response code="404">Not Found</response>
        /// <response code="500">Error</response>
        [HttpPut("{id}")]
        public async  Task<IActionResult>  Update(int id, Building building){

             Building expectedBuilding = await Task.Run( () =>
                context.Buildings.Find(id)
            );

           
        if (id != expectedBuilding.BuildingId)
          {
              return BadRequest();
          }

          Building newBuilding = await context.Buildings.FindAsync(id);
          if (newBuilding == null)
          {
              return NotFound();
          }

            newBuilding.Name = building.Name;
            newBuilding.Street = building.Street;
            newBuilding.Town = building.Town;
            newBuilding.Postcode = building.Postcode;
            newBuilding.FloorsNumber = building.FloorsNumber;
            newBuilding.Country = building.Country;
            newBuilding.Floors = building.Floors;
          

          try
          {
              await context.SaveChangesAsync();
              return StatusCode(200);

          }
          catch (DbUpdateConcurrencyException)
          {
              return NotFound();
          }

        }



          /// <summary>
        ///  Delete  a specific building by ID
        /// </summary>
        /// <param name="id"> Building ID</param>
        /// <response code="200">Sucess</response>
        /// <response code="404">Not found</response>
        /// <response code="500">Error</response>
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id){
            Building expectedBuilding = await Task.Run( () =>
                context.Buildings.Find(id)
            );

            if(expectedBuilding is null){
                return NotFound();
            }



            await Task.Run( () => context.Buildings.Remove(expectedBuilding));
            context.SaveChanges();
             return StatusCode(200);
        }


    }
}
