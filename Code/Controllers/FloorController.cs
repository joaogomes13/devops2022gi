using System.Collections.Generic;
using Code.Contexts;
using Code.Models;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Code.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FloorController : ControllerBase
    {

         private BackendContext context;

        public FloorController()
        {
             context = new BackendContext();
          
        }


        /// <summary>
        ///  Get all the floors
        /// </summary>
        /// <response code="200">Sucess</response>
        /// <response code="500">Error</response>
        ///
        [HttpGet]
        public async Task<IEnumerable<Floor>> Index()    
        {

             List<Floor> floors = new List<Floor>();

             floors = await Task.Run( () => context.Floors.ToList());

             return floors;

        }

          /// <summary>
        ///  Get a specific floor by ID
        /// </summary>
        /// <param name="id"> Floor ID</param>
        /// <response code="200">Sucess</response>
        /// <response code="400">Not found</response>
        /// <response code="500">Error</response>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id){

           Floor floor =  await Task.Run(() =>  context.Floors.Find(id));

            if(floor == null)
                return NotFound();
            
            return Ok(floor) ;

        }



        /// <summary>
        /// Create Floor
        /// </summary>
        /// <param name="floor"> Floor Model</param>
        /// <response code="200">Sucess</response>
        /// <response code="400">Invalid Floor Model</response>
        /// <response code="404">Not found</response>
        /// <response code="500">Error</response>
        [HttpPost]
        public async Task<ActionResult> Create(Floor floor){

            await Task.Run(() =>  context.Floors.Add(floor));
            context.SaveChanges();
            return CreatedAtAction(
                nameof(Create),
                new {
                id = floor.Id
                },
                floor
            );

        }



        /// <summary>
        /// Edit  specific floor by ID
        /// </summary>
        /// <param name="id"> Floor ID</param>
        /// <param name="floor"> Floor Model </param>
        /// <response code="200">Sucess</response>
        /// <response code="400">Invalid Floor Model</response>
        ///  <response code="404">Not Found</response>
        /// <response code="500">Error</response>
        [HttpPut("{id}")]
        public async  Task<IActionResult>  Update(int id, Floor floor){

             Floor expectedFloor = await Task.Run( () =>
                context.Floors.Find(id)
            );

           
        if (id != expectedFloor.Id)
          {
              return BadRequest();
          }

          Floor newFloor = await context.Floors.FindAsync(id);
          if (newFloor == null)
          {
              return NotFound();
          }
            
            newFloor.Type = floor.Type;
            newFloor.Number = floor.Number;
            newFloor.NumberOfRooms = floor.NumberOfRooms;
            newFloor.BuildingId = floor.BuildingId;
            newFloor.Building = floor.Building;
            newFloor.Flats = floor.Flats;

          try
          {
              await context.SaveChangesAsync();
              return StatusCode(200);

          }
          catch (DbUpdateConcurrencyException)
          {
              return NotFound();
          }

        }



          /// <summary>
        ///  Delete  a specific floor by ID
        /// </summary>
        /// <param name="id"> Floor ID</param>
        /// <response code="200">Sucess</response>
        /// <response code="404">Not found</response>
        /// <response code="500">Error</response>
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id){
            Floor expectedFloor = await Task.Run( () =>
                context.Floors.Find(id)
            );

            if(expectedFloor is null){
                return NotFound();
            }



            await Task.Run( () => context.Floors.Remove(expectedFloor));
            context.SaveChanges();
             return StatusCode(200);
        }


    }
}