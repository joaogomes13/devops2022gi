using System.Collections.Generic;
using Code.Contexts;
using Code.Models;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Code.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FlatController : ControllerBase
    {

         private BackendContext context;

        public FlatController()
        {
             context = new BackendContext();
          
        }


        /// <summary>
        ///  Get all the flats
        /// </summary>
        /// <response code="200">Sucess</response>
        /// <response code="500">Error</response>
        ///
        [HttpGet]
        public async Task<IEnumerable<Flat>> Index()    
        {

             List<Flat> flats = new List<Flat>();

             flats = await Task.Run( () => context.Flats.ToList());

             return flats;

        }

          /// <summary>
        ///  Get a specific flat by ID
        /// </summary>
        /// <param name="id"> Flat ID</param>
        /// <response code="200">Sucess</response>
        /// <response code="400">Not found</response>
        /// <response code="500">Error</response>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id){

           Flat flat =  await Task.Run(() =>  context.Flats.Find(id));

            if(flat == null)
                return NotFound();
            
            return Ok(flat) ;

        }



        /// <summary>
        /// Create Flat
        /// </summary>
        /// <param name="flat"> Flat Model</param>
        /// <response code="200">Sucess</response>
        /// <response code="400">Invalid Flat Model</response>
        /// <response code="404">Not found</response>
        /// <response code="500">Error</response>
        [HttpPost]
        public async Task<ActionResult> Create(Flat flat){

            await Task.Run(() =>  context.Flats.Add(flat));
            context.SaveChanges();
            return CreatedAtAction(
                nameof(Create),
                new {
                id = flat.Id
                },
                flat
            );

        }



        /// <summary>
        /// Edit  specific flat by ID
        /// </summary>
        /// <param name="id"> Flat ID</param>
        /// <param name="flat"> Flat Model </param>
        /// <response code="200">Sucess</response>
        /// <response code="400">Invalid Flat Model</response>
        ///  <response code="404">Not Found</response>
        /// <response code="500">Error</response>
        [HttpPut("{id}")]
        public async  Task<IActionResult>  Update(int id, Flat flat){

             Flat expectedFlat = await Task.Run( () =>
                context.Flats.Find(id)
            );

           
        if (id != expectedFlat.Id)
          {
              return BadRequest();
          }

          Flat newFlat = await context.Flats.FindAsync(id);
          if (newFlat == null)
          {
              return NotFound();
          }

            newFlat.SquareMeters = flat.SquareMeters;
            newFlat.NumberOfRooms = flat.NumberOfRooms;
            newFlat.OwnerId = flat.OwnerId;
            newFlat.Owner = flat.Owner;
            newFlat.Floor = flat.Floor;
          

          try
          {
              await context.SaveChangesAsync();
              return StatusCode(200);

          }
          catch (DbUpdateConcurrencyException)
          {
              return NotFound();
          }

        }



          /// <summary>
        ///  Delete  a specific flat by ID
        /// </summary>
        /// <param name="id"> Flat ID</param>
        /// <response code="200">Sucess</response>
        /// <response code="404">Not found</response>
        /// <response code="500">Error</response>
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id){
            Flat expectedFlat = await Task.Run( () =>
                context.Flats.Find(id)
            );

            if(expectedFlat is null){
                return NotFound();
            }



            await Task.Run( () => context.Flats.Remove(expectedFlat));
            context.SaveChanges();
             return StatusCode(200);
        }


    }
}