using System.Collections.Generic;
using System.Threading.Tasks;
using Code.Contexts;
using Code.Models;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Code.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class OwnerController : ControllerBase
  {

   private BackendContext context;

    public OwnerController()
    {
     context = new BackendContext();
    }

    /// <summary>
    ///  Get all the owners
    /// </summary>
    /// <response code="200">Sucess</response>
    /// <response code="500">Error</response>
    [HttpGet]
    public async Task<IEnumerable<Owner>> Index()
    {
      List<Owner> owners = new List<Owner>();

      owners = await Task.Run( () => context.Owners.ToList());

      return owners;
    }


    /// <summary>
    ///  Get a specific owner by ID
    /// </summary>
    /// <param name="id"> Owner ID</param>
    /// <response code="200">Sucess</response>
    /// <response code="400">Not found</response>
    /// <response code="500">Error</response>
    [HttpGet("{id}")]
    public async Task<IActionResult> GetById(int id)
    {
      Owner owner =  await Task.Run(() =>  context.Owners.Find(id));

            if(owner == null)
                return NotFound();

            return Ok(owner) ;
    }

    /// <summary>
    /// Create Owner
    /// </summary>
    /// <param name="owner"> Owner Model</param>
    /// <response code="200">Sucess</response>
    /// <response code="400">Invalid Owner Model</response>
    /// <response code="404">Not found</response>
    /// <response code="500">Error</response>
    [HttpPost]
    public async Task<ActionResult> Create(Owner owner)
    {

     await Task.Run(() =>  context.Owners.Add(owner));
            context.SaveChanges();

            return CreatedAtAction(
                nameof(Create),
                new {
                id = owner.OwnerId
                },
                owner
            );

      
    }

    /// <summary>
    /// Edit  specific Owner by ID
    /// </summary>
    /// <param name="id"> Owner ID</param>
    /// <param name="owner"> Owner Model </param>
    /// <response code="200">Sucess</response>
    /// <response code="400">Invalid Owner Model</response>
    ///  <response code="404">Not Found</response>
    /// <response code="500">Error</response>
    [HttpPut("{id}")]
    public async Task<IActionResult> Update(int id, Owner owner)
    {
     
        Owner expectedOwner = await Task.Run( () =>
                context.Owners.Find(id)
            );

           
        if (id != expectedOwner.OwnerId)
          {
              return BadRequest();
          }

          var newOwner = await context.Owners.FindAsync(id);
          if (newOwner == null)
          {
              return NotFound();
          }

            newOwner.FirstName = owner.FirstName;
            newOwner.LastName = owner.LastName;
            newOwner.PhoneNumber = owner.PhoneNumber;
            newOwner.Flats = owner.Flats;

          try
          {
              await context.SaveChangesAsync();
          }
          catch (DbUpdateConcurrencyException)
          {
              return NotFound();
          }
      return StatusCode(200);

    }

    /// <summary>
    ///  Delete  a specific Owner by ID
    /// </summary>
    /// <param name="id"> Owner ID</param>
    /// <response code="200">Sucess</response>
    /// <response code="404">Not found</response>
    /// <response code="500">Error</response>
    [HttpDelete("{id}")]
    public async Task<ActionResult> Delete(int id)
    {
       Owner expectedOwner = await Task.Run( () =>
                context.Owners.Find(id)
            );

            if(expectedOwner is null){
                return NotFound();
            }



            await Task.Run( () => context.Owners.Remove(expectedOwner));
            context.SaveChanges();
             return StatusCode(200);
    }


  }
}

