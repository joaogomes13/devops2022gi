using Microsoft.EntityFrameworkCore;
using Code.Models;
using System;

namespace Code.Contexts
{
    public class BackendContext : DbContext
    {
        
        public BackendContext()
        {
        }

         public DbSet<Floor> Floors { get; set; }
         public DbSet<Building> Buildings { get; set; }
         public DbSet<Flat> Flats { get; set; }
         public DbSet<Owner> Owners { get; set; }

         protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
         {
            try
            {
                 optionsBuilder.UseSqlServer(@"Server=sql1;Database=DevOps;User Id=SA;Password=Vagrant42");
            }
            catch (System.Exception)
            {
                
                Console.WriteLine("Connection");
            }
            

         }
    }
}